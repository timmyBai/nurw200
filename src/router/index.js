import Vue from 'vue';
import VueRouter from 'vue-router';

import Layout from '@/layout'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/inpAss/index',  //入院護理評估(包含成人、兒科、婦科、產科、新生兒、身心科)
    component: Layout,
    children: [
      {
        path: 'inpAss/index/:hsp/:crtno/:crtseq/:opid', //綁定參數 hsp 院區、crtno 病歷號、crtseq 流水號、opid 員編
        name: 'inpAss',
        component: () => import('@/views/inpAss/index')
      }
    ]
  },
  {
    path: '/everyAss',
    redirect: '/everyAss/index', //每日身體評估
    component: Layout,
    children: [
      {
        path: 'index/:hsp/:crtno/:crtseq/:opid',  //綁定參數 hsp 院區、crtno 病歷號、crtseq 流水號、opid 員編
        name: 'everyAss',            //每日身體評估
        component: () => import('@/views/everyAss/index')
      }
    ]
  },
  {
    path: '/nursingGuideAss',
    redirect: '/nursingGuideAss/index',  //護理指導評估
    component: Layout,
    children: [
      {
        path: 'index/:hsp/:crtno/:crtseq/:opid', //綁定參數 hsp 院區、crtno 病歷號、crtseq 流水號、opid 員編
        name: 'nursingGuid',  //護理指導評估
        component: () => import('@/views/nursingGuideAss/index')
      }
    ]
  },
  {
    path: '/nurTraint',
    redirect: '/nurTraint/index',    //護理約束登入
    component: Layout,
    children: [
      {
        path: 'index/:hsp/:crtno/:crtseq/:opid',     //綁定參數 hsp 院區、crtno 病歷號、crtseq 流水號、opid 員編
        name: 'nurTraint', //約束登錄
        component: () => import('@/views/nurTraint/index')
      }
    ]
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  },
  {
    path: '*',
    redirect: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  }
];

const router = new VueRouter({
  routes
});

export default router;
