import request from '@/utils/request';  //引入 request 攔截 api 回傳訊息

//入院護理評估 (包括成人、兒科、婦科、產科、新生兒、身心科) api
export function getNurInHos(hsp, crtno, crtseq, opid){
    return request({
        url: 'http://test.com.tw/NU/NURP5000_WEBAPI/api/NURP5000/Get_NUR_InHos/',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            crtseq: crtseq,
            opid: opid
        }
    });
}

//每日身體評估(每日清單) api
export function getNurDayRList(hsp, crtno, crtseq, opid){
    return request({
        url: 'http://test.com.tw/NU/NURP5000_WEBAPI/api/NURP5000/Get_NUR_DayRList/',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            crtseq: crtseq,
            opid: opid
        }
    });
}

//每日身體評估(當日詳細清單) api
export function getNurDayRec(hsp, crtno, crtseq, vsdte, vstme, opid){
    return request({
        url: 'http://test.com.tw/NU/NURP5000_WEBAPI/api/NURP5000/Get_NUR_DayRec/',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            crtseq: crtseq,
            vsdte: vsdte,
            vstme: vstme,
            opid: opid
        }
    });
}

//護理指導評估 api
export function getNurGuideList(hsp, crtno, crtseq, opid)
{
    return request({
        url: 'http://test.com.tw/NU/NURP5000_WEBAPI/api/NURP5000/Get_NUR_GuideList/',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            crtseq: crtseq,
            opid: opid
        }
    });
}

//護理約束登入清單 api
export function getNurTraintList(hsp, crtno, crtseq, opid)
{
    return request({
        url: 'http://test.com.tw/NU/NURP5000_WEBAPI/api/NURP5000/Get_NUR_TraintList/',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            crtseq: crtseq,
            opid: opid
        }
    });
}


//護理約束登入內容與查核表清單
export function getNurTraintCheckContent(hsp, crtno, crtseq, sdte, stme, edte, etme, opid)
{
    return request({
        url: 'http://test.com.tw/NU/NURP5000_WEBAPI/api/NURP5000/Get_NUR_TraintCheck/',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            crtseq: crtseq,
            sdte: sdte,
            stme: stme,
            edte: edte,
            etme: etme,
            opid: opid
        }
    });
}

//護理約束登入查核明細
export function getNurTraintChkInfo(hsp, crtno, crtseq, sdte, stme, evldte, evltme, opid)
{
    return request({
        url: 'http://test.com.tw/NU/NURP5000_WEBAPI/api/NURP5000/Get_NUR_Traint_ChkInfo/',
        method: 'get',
        params: {
            hsp: hsp,
            crtno: crtno,
            crtseq: crtseq,
            sdte: sdte,
            stme: stme,
            evldte: evldte,
            evltme: evltme,
            opid: opid
        }
    });
}