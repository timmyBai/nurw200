import axios from 'axios';
import qs from 'qs';
import store from '@/store';

const service = axios; //將 axios 儲存至變數

service.interceptors.request.use(
    config => {
        store.dispatch('app/setLoading', true);

        if (config.method === 'post' || config.method === 'POST')
        {
            config.data = qs.stringify(config.data);
        }

        return config
    }
)

service.interceptors.response.use( //使用 axios 攔截器，攔截 response 回傳所有結果
    response => {
        store.dispatch('app/setLoading', false);

        if (response.request.readyState === 4 && response.status === 200) //如果攔截回來有產生標頭，且 status code 狀態為 200 成功狀態
        {
            return Promise.resolve(response); //回傳 response 資料
        }
    },
    error => {
        let serverErrorMessage = null; //儲存 api 發生什麼錯誤

        if (error && error.response)  //如果 error 和 error.response 存在時
        {
            switch(error.response.status)  //判斷 status code 錯誤狀態
            {
                case 401:                                         //如果遇到 401 狀態
                    serverErrorMessage = '401';          // 儲存 401 ERROR 陣列
                    break;                                            //中止
                case 403:                                         //如果遇到 403 狀態
                    serverErrorMessage = '403';          //儲存 401 ERROR 陣列
                    break;                                            //中止
                case 404:                                         //如果遇到 404 狀態
                    serverErrorMessage = '找不到頁面';    // 儲存 404 ERROR 陣列
                    break;                                            //中止
                case 500:                                         //如果遇到 500 狀態
                    serverErrorMessage = '伺服器錯誤';    //儲存 500 ERROR 陣列
                    break;                                            //中止
                case 503:                                         //如果遇到 503 狀態
                    serverErrorMessage = '服務失敗';      //儲存 503 ERROR 陣列
                    break;                                            //中止
                default:                                          //其他
                    serverErrorMessage = '連接錯誤';      //回傳 "連接錯誤" ERROR 陣列
                    break;
            }
        }
        else  //否則什麼都沒有
        {
            serverErrorMessage = '連接服務失敗';   //回傳 "連接服務失敗" ERROR 陣列
        }

        store.dispatch('app/setLoading', false);

    return Promise.resolve(serverErrorMessage);   //回傳 serverErrorMessage 變數，最終錯誤訊息
});

export default service;