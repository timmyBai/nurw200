import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

//載入套件
//style部分
import 'normalize.css';
import 'bootstrap/scss/bootstrap.scss'; //bootstrap 樣式
import '@/styles/reset.sass'; //自定義重設樣式

//js 部分
import 'jquery';
import 'popper.js';

//元件部分
import Loading from './components/Loading';

//vue 共用元件部分
Vue.component('Loading', Loading);


Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
