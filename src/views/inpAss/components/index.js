export { default as AdultAss } from './AdultAss.vue'; //成人
export { default as PediatricsAss } from './PediatricsAss.vue'; //兒科
export { default as GynecologicalAss } from './GynecologicalAss.vue'; //婦科
export { default as ObstetricsAss } from './ObstetricsAss.vue'; //產科
export { default as NewbornAss } from './NewbornAss.vue';      //新生兒
export { default as MindAndBodyAss } from './MindAndBodyAss.vue'   //身心科